import { datCuocAction } from '../actions/XucXacActions';
import { dat_cuoc, play_game } from '../types/XucXacTypes';

const initialState = {
  taiXiu: true, //true là tài (tổng điểm lớn hơn 11), false là xỉu (tổng điểm từ 3-11)
  xucXacArr: [
    { ma: 1, hinhAnh: './img/imgxucxac/1.png' },
    { ma: 1, hinhAnh: './img/imgxucxac/1.png' },
    { ma: 1, hinhAnh: './img/imgxucxac/1.png' },
  ],
  soBanThang: 0,
  soLuotChoi: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case dat_cuoc: {
      // kiểu dữ liệu cơ sở thì không cần clone ra một object mới, số hay chuỗi thì có thẻ gán trực tiếp được, đó là vì tính bất biến của javascript
      state.taiXiu = action.taiXiu;
      return {
        ...state,
      };
    }
    case play_game: {
      console.log('yes');

      // Bước 1: xử lý random xúc xắc
      let xucXacArrRandom = [];
      for (let i = 0; i < state.xucXacArr.length; i++) {
        // Mỗi lần lặp random ra số ngẫu nhiên từ 1 tới 6
        let soNgauNhien = Math.floor(Math.random() * 6) + 1;
        // tạo ra một đối tượng xúc xắc từ số ngẫu nhiên
        let xucXacNgauNhien = {
          ma: soNgauNhien,
          hinhAnh: `./img/imgxucxac/${soNgauNhien}.png`,
        };
        // push vào mảng xúc xắc ngẫu nhiên
        xucXacArrRandom.push(xucXacNgauNhien);
      }
      // gán state xucXacArr = xucXacArrRandom
      state.xucXacArr = xucXacArrRandom;
      // xử lý bàn chơi
      state.soLuotChoi += 1;
      // xử lý win game
      let tongSoDiem = xucXacArrRandom.reduce((tongDiem, xucXac, index) => {
        return (tongDiem += xucXac.ma);
      }, 0);
      if (
        (tongSoDiem > 11 && state.taiXiu) ||
        (tongSoDiem <= 11 && !state.taiXiu)
      ) {
        state.soBanThang += 1;
      }
      return { ...state };
    }
    default:
      return state;
  }
};
