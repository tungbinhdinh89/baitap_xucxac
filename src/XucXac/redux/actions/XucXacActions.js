import { dat_cuoc, play_game } from '../types/XucXacTypes';
export const datCuocAction = (taiXiu) => ({
  type: dat_cuoc,
  taiXiu,
});
export const playGameAction = (payload) => ({
  type: play_game,
  payload,
});
