import React, { Component } from 'react';
import './GameXucXac.css';
import ImageXucXac from './ImageXucXac';
import ThongTinTroChoi from './ThongTinTroChoi';
import { connect } from 'react-redux';
import { datCuocAction, playGameAction } from './redux/actions/XucXacActions';

class XucXac extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center mt-5 display-4">
          Game Xúc Xắc
        </div>
        <div className="row text-center mt-5">
          <div className="col-5">
            <button
              className="btnGame btn btn-success"
              onClick={() => {
                this.props.dispatch(datCuocAction(true));
              }}>
              Tài
            </button>
          </div>
          <div className="col-2">
            <ImageXucXac />
          </div>
          <div className="col-5">
            <button
              className=" btnGame btn btn-primary"
              onClick={() => {
                this.props.dispatch(datCuocAction(false));
              }}>
              Xỉu
            </button>
          </div>
        </div>
        <ThongTinTroChoi />
        <button
          className="btn btn-success mt-4 p-4 rounded"
          onClick={() => {
            this.props.dispatch(playGameAction());
          }}>
          Play Game
        </button>
      </div>
    );
  }
}

export default connect(null, null)(XucXac);
