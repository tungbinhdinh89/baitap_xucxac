import React, { Component } from 'react';
import XucXacReducer from './redux/reducers/XucXacReducer.';
import { connect } from 'react-redux';
class ImageXucXac extends Component {
  renderXucXac = () => {
    // lấy props từ reducer vể
    return this.props.xucXacArr.map((item, index) => {
      return (
        <img
          key={index}
          className="ml-2"
          style={{ width: 80, height: 80 }}
          src={item.hinhAnh}
          alt={item.hinhAnh}
        />
      );
    });
  };

  render() {
    return <div>{this.renderXucXac()}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    xucXacArr: state.XucXacReducer.xucXacArr,
  };
};
export default connect(mapStateToProps)(ImageXucXac);
