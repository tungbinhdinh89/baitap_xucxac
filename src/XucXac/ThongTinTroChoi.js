import React, { Component } from 'react';
import { connect } from 'react-redux';
import XucXacReducer from './redux/reducers/XucXacReducer.';

class ThongTinTroChoi extends Component {
  render() {
    return (
      <div>
        <h1 className="display-4">
          Bạn Chọn:{' '}
          <span className="text-danger">
            {this.props.taiXiu ? 'Tài' : 'Xỉu'}
          </span>
        </h1>
        <h1 className="display-4">
          Số Bàn Thắng:
          <span className="text-success">{this.props.soBanThang}</span>
        </h1>
        <h1 className="display-4">
          Lượt Chơi:
          <span className="text-success">{this.props.soLuotChoi}</span>
        </h1>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    taiXiu: state.XucXacReducer.taiXiu,
    soBanThang: state.XucXacReducer.soBanThang,
    soLuotChoi: state.XucXacReducer.soLuotChoi,
  };
};
export default connect(mapStateToProps)(ThongTinTroChoi);
